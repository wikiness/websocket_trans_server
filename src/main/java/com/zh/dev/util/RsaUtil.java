package com.zh.dev.util;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;

@Component
@ConfigurationProperties(prefix = "mrsa")
/** 非对称加密工具*/
public class RsaUtil {
	
	private static String pub;
	
	private static String priv;
	
	private static RSA rsa;
	
	public void setPriv(String priv) {
		RsaUtil.priv = priv;
	}
	
	public void setPub(String pub) {
		RsaUtil.pub = pub;
	}
	
	/** 初始化加密工具 */
	private static void init() {
		if(rsa == null) {
			rsa = new RSA(priv, pub);
		}
	}
	
	/** 加密，返回hex字符串 */
	public static String encodeHex(String msg) {
		init();
		return rsa.encryptHex(msg, KeyType.PublicKey);
	}
	
	/** 解密hex字符串,返回解密字符串 */
	public static String decodeHex(String hex) {
		init();
		return rsa.decryptStr(hex, KeyType.PrivateKey);
	}
	
}
