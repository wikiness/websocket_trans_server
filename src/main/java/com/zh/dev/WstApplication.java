package com.zh.dev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class WstApplication {

    public static void main(String[] args) {
        SpringApplication.run(WstApplication.class, args);
    }

}
